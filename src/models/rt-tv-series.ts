export type RTTVSeries = {
	title: string;
	startYear: number | null;
	url: string;
	meterScore: number | null;
};
