import type {
	Media,
	MediaSet,
	MediaType
} from '@rt_lite/common/models';

export type Parser = {
	/* eslint-disable @typescript-eslint/prefer-readonly-parameter-types */
	parseFrontPageListItem: <T extends MediaType> (item: HTMLLIElement) => Media<T> | null;
	parseFrontPageList: <T extends MediaType> (list: HTMLUListElement) => MediaSet<T> | null;
	parseDynamicListItem: <T extends MediaType> (listItem: HTMLDivElement) => Media<T> | null;
	parseDynamicList: <T extends MediaType> (list: HTMLDivElement) => MediaSet<T> | null;
	parseFrontPage: <T extends MediaType> (body: HTMLBodyElement) => MediaSet<T>[];
	/* eslint-enable @typescript-eslint/prefer-readonly-parameter-types */
};
