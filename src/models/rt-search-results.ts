import type { RTMovie } from '~/models/rt-movie';
import type { RTTVSeries } from '~/models/rt-tv-series';

export type RTSearchResults = {
	movies: RTMovie[];
	tvSeries: RTTVSeries[];
};
