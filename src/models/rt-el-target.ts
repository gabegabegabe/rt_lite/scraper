export enum RtElTarget {
	TITLE = 'span.dynamic-text-list__item-title',
	SCORE = 'span[slot="tomatometer-value"]',
	FRONT_PAGE_MOVIE_LIST = 'section.dynamic-text-list',
	FRONT_PAGE_DYNAMIC_LIST = 'section.dynamic-poster-list',
	LIST_TITLE = 'h2[slot="header"]',
	LIST_ITEM = 'ul[slot="list-items"] li',
	DYNAMIC_LIST_TITLE = 'h2',
	DYNAMIC_LIST_ITEM = 'tile-dynamic',
	DYNAMIC_LIST_ITEM_LINK = 'a[slot="caption"]',
	DYNAMIC_LIST_ITEM_TITLE = 'button.js-show-modal-trailer span.sr-only',
	DYNAMIC_LIST_ITEM_SCORE = 'score-pairs'
}
