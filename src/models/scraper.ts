import type { SearchResults } from '~/models/search-results';
import type {
	MediaSet,
	MediaType,
	Movie,
	TVSeries
} from '@rt_lite/common';

export type Scraper = {
	findMedia: (terms?: string) => Promise<SearchResults>;
	findMovies: (terms?: string) => Promise<Movie[]>;
	findTVSeries: (terms?: string) => Promise<TVSeries[]>;
	getFrontPageSets: () => Promise<MediaSet<MediaType>[]>;
};
