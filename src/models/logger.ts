type LogFunction = (...msgs: readonly unknown[]) => void;

export type Logger = {
	log: LogFunction;
	error: LogFunction;
	warn: LogFunction;
	info: LogFunction;
	success: LogFunction;
};
