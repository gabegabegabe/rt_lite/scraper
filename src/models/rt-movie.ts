export type RTMovie = {
	name: string;
	year: number | null;
	meterScore: number | null;
	url: string;
};
