type ImmutableArray<T> = readonly Immutable<T>[];

type ImmutableObject<T> = {
	readonly [P in keyof T]: Immutable<T[P]>;
};

export type Immutable<T> =
	T extends (infer R)[] ? ImmutableArray<R> :
		// eslint-disable-next-line @typescript-eslint/ban-types
		T extends Function ? T :

			T extends object ? ImmutableObject<T> :
				T;
