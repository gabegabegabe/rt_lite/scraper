import type { Immutable } from '~/models/immutable';

type RequestOptions = {
	method?: string;
	headers?: Record<string, string>;
};

type RequestPayload <T> = {
	json: () => Promise<T>;
	text: () => Promise<string>;
};

export type Fetcher = <T> (url: string, options?: Immutable<RequestOptions>) => Promise<RequestPayload<T>>;
