import type {
	Movie,
	TVSeries
} from '@rt_lite/common';

export type SearchResults = {
	movies: Movie[];
	tvSeries: TVSeries[];
};
