export type RTMedia = {
	meterScore: number | null;
	url: string;
};
