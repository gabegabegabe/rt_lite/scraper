import { createScraper as createGenericScraper } from '~/factories/scraper';
import { createLogger } from '~/factories/logger';
import type { Fetcher } from '~/models/fetcher';
import type { Immutable } from '~/models/immutable';
import { JSDOM } from 'jsdom';
import type { Logger } from '~/models/logger';
import nodeFetch from 'node-fetch';
import type { Scraper } from '~/models/scraper';

const bodyProducer = (html: string): HTMLBodyElement | null => {
	const { window: { document } } = new JSDOM(html);

	const body = document.querySelector<HTMLBodyElement>('body');

	return body;
};

const fetcher = nodeFetch as unknown as Fetcher;

export const createScraper = ({
	logger = console,
	shouldLog = true
}: Immutable<{
	logger?: Partial<Logger> & Pick<Logger, 'log'>;
	shouldLog?: boolean;
}> = {
	logger: console,
	shouldLog: true
}): Scraper => createGenericScraper({
	bodyProducer,
	fetcher,
	logger: createLogger(logger),
	shouldLog
});
