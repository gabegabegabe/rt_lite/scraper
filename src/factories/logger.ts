import type { Logger } from '~/models/logger';

export const createLogger = (
	logService: Readonly<Partial<Logger> & Pick<Logger, 'log'>> = console
): Logger => ([
	'log',
	'error',
	'warn',
	'info',
	'success'
] as const)
	.map(key => [
		key,
		(...msgs: readonly unknown[]): void => {
			(logService[key] ?? logService.log)(...msgs);
		}
	] as const)
	// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
	.reduce((logger, [key, fn]) => ({
		...logger,
		[key]: fn
	// eslint-disable-next-line @typescript-eslint/prefer-reduce-type-parameter
	}), {} as Logger);
