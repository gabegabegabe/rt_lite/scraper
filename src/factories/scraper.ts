import { createLogger } from '~/factories/logger';
import { createMovieFromRTMovie } from '~/factories/movie-from-rt-movie';
import { createParser } from '~/factories/parser';
import { createTVSeriesFromRTTVSeries } from '~/factories/tv-series-from-rt-tv-series';
import type { Fetcher } from '~/models/fetcher';
import type { Immutable } from '~/models/immutable';
import type { Logger } from '~/models/logger';
import { RT_SEARCH_URL } from '~/core/rt-search-url';
import { RT_URL } from '~/core/rt-url';
import type { RTMovie } from '~/models/rt-movie';
import type { RTSearchResults } from '~/models/rt-search-results';
import type { RTTVSeries } from '~/models/rt-tv-series';
import type { Scraper } from '~/models/scraper';
import type { SearchResults } from '~/models/search-results';
import { trimNonBodyHtml } from '~/utilities/trim-non-body-html';
import type {
	MediaSet,
	MediaType,
	Movie,
	TVSeries
} from '@rt_lite/common';

const USER_AGENT = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1' as const;
// eslint-disable-next-line @typescript-eslint/naming-convention
const HEADERS = { 'User-Agent': USER_AGENT } as const;

export const createScraper = ({
	bodyProducer,
	fetcher,
	logger = createLogger(console),
	shouldLog = true
}: Immutable<{
	bodyProducer: (html: string) => HTMLBodyElement | null;
	fetcher: Fetcher;
	logger?: Readonly<Logger>;
	shouldLog?: boolean;
}>): Scraper => {
	const parser = createParser();

	const getFrontPageBody = async (): Promise<HTMLBodyElement | null> => {
		try {
			const response = await fetcher(RT_URL.href, {
				method: 'GET',
				headers: HEADERS
			});

			const page = await response.text();
			const bodyHtml = trimNonBodyHtml(page);
			const body = bodyProducer(bodyHtml);

			return body;
		} catch (cause) {
			if (shouldLog) logger
				.error(new Error('Failed to get front page', { cause }));

			return null;
		}
	};

	const findMedia = async (terms?: string): Promise<SearchResults> => {
		if (typeof terms !== 'string') return { movies: [], tvSeries: [] };

		const searchUrl = new URL('#', RT_SEARCH_URL);
		searchUrl.searchParams.append('query', terms);

		try {
			const response = await fetcher<RTSearchResults>(searchUrl.href, { method: 'GET', headers: HEADERS });
			const { movies, tvSeries } = await response.json();

			return {
				movies: movies.map((movie: Readonly<RTMovie>) => createMovieFromRTMovie(movie)),
				tvSeries: tvSeries.map((series: Readonly<RTTVSeries>) => createTVSeriesFromRTTVSeries(series))
			};
		} catch (cause) {
			if (shouldLog) logger.error(new Error('Failed to find media', { cause }));

			return { movies: [], tvSeries: [] };
		}
	};

	const findMovies = async (terms?: string): Promise<Movie[]> => (await findMedia(terms)).movies;

	const findTVSeries = async (terms?: string): Promise<TVSeries[]> => (await findMedia(terms)).tvSeries;

	const getFrontPageSets = async (): Promise<MediaSet<MediaType>[]> => {
		const body = await getFrontPageBody();

		if (!body) return [];

		return parser.parseFrontPage(body);
	};

	return {
		findMedia,
		findMovies,
		findTVSeries,
		getFrontPageSets
	};
};
