import type { Parser } from '~/models/parser';
import { RT_URL } from '~/core/rt-url';
import { RtElTarget } from '~/models/rt-el-target';
import type {
	Media,
	MediaSet,
	MediaType
} from '@rt_lite/common/models';

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
const parseFrontPageListItem = <T extends MediaType> (item: HTMLLIElement): Media<T> | null => {
	const titleSpan = item.querySelector<HTMLSpanElement>(RtElTarget.TITLE);
	if (!titleSpan) return null;

	// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
	const title = titleSpan.textContent!;

	const urlLink = item.querySelector<HTMLAnchorElement>('a');
	if (!urlLink) return null;

	const href = urlLink.getAttribute('href');
	if (href === null) return null;

	const url = new URL(href, RT_URL);

	const movie: Media<T> = { title, url };

	const scoreSpan = item.querySelector<HTMLSpanElement>(RtElTarget.SCORE);

	if (!scoreSpan) return movie;

	const score = Number(scoreSpan.textContent?.replace('%', ''));

	if (isNaN(score)) return movie;

	return { ...movie, score };
};

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
const parseFrontPageList = <T extends MediaType> (list: HTMLUListElement): MediaSet<T> | null => {
	const heading = list.querySelector<HTMLHeadingElement>(RtElTarget.LIST_TITLE);
	if (!heading) return null;

	const mediaListItems = [...list.querySelectorAll<HTMLLIElement>(RtElTarget.LIST_ITEM)];
	if (mediaListItems.length < 1) return null;

	const { textContent: title } = heading;
	if (title === null || title.length < 1) return null;

	const media = mediaListItems
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		.map(listItem => parseFrontPageListItem(listItem))
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		.filter(mediaItem => mediaItem !== null) as Media<T>[];

	return { title, media };
};

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
const parseDynamicListItem = <T extends MediaType> (listItem: HTMLDivElement): Media<T> | null => {
	const titleEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_TITLE);
	if (!titleEl) return null;

	const link = listItem.querySelector<HTMLAnchorElement>(RtElTarget.DYNAMIC_LIST_ITEM_LINK);
	if (!link) return null;

	const { textContent: title } = titleEl;
	if (title === null || title === '') return null;

	const href = link.getAttribute('href');
	if (href === null) return null;

	const url = new URL(href, RT_URL);

	const movie: Media<T> = { title, url };

	const scoreEl = listItem.querySelector<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE);
	if (!scoreEl) return movie;

	const scoreString = scoreEl.getAttribute('criticsscore');
	if (scoreString === null || scoreString.length === 0) return movie;
	const score = Number(scoreString);

	if (isNaN(score)) return movie;

	return { ...movie, score };
};

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
const parseDynamicList = <T extends MediaType> (list: HTMLDivElement): MediaSet<T> | null => {
	const heading = list.querySelector<HTMLHeadingElement>(RtElTarget.DYNAMIC_LIST_TITLE);
	if (!heading) return null;

	const mediaListItems = [...list.querySelectorAll<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM)];
	if (mediaListItems.length < 1) return null;

	const { textContent: title } = heading;
	if (title === null || title.length <= 0) return null;

	const media = mediaListItems
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		.map(listItem => parseDynamicListItem(listItem))
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		.filter(mediaItem => mediaItem !== null) as Media<T>[];

	return { title, media };
};

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
const parseFrontPage = <T extends MediaType> (body: HTMLBodyElement): MediaSet<T>[] => {
	const frontPageDynamicMovieSets = [...body.querySelectorAll<HTMLDivElement>(RtElTarget.FRONT_PAGE_DYNAMIC_LIST)]
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		.map(list => parseDynamicList(list));

	const frontPageListMovieSets = [...body.querySelectorAll<HTMLUListElement>(RtElTarget.FRONT_PAGE_MOVIE_LIST)]
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		.map(list => parseFrontPageList(list));

	return [...frontPageDynamicMovieSets, ...frontPageListMovieSets]
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		.filter(movieSet => movieSet !== null) as MediaSet<T>[];
};

export const createParser = (): Parser => ({
	parseDynamicList,
	parseDynamicListItem,
	parseFrontPage,
	parseFrontPageList,
	parseFrontPageListItem
});
