import { createParser } from '~/factories/parser';
import frontPageMediaSets from '~/testing/front-page.json';
import type { Parser } from '~/models/parser';
import path from 'path';
import { readFile } from 'fs/promises';
import { RT_URL } from '~/core/rt-url';
import { RtElTarget } from '~/models/rt-el-target';
import { trimNonBodyHtml } from '~/utilities/trim-non-body-html';
import {
	DEFAULT_LIST_LENGTH,
	generateDynamicList,
	generateDynamicListItem
} from '~/testing/rt-dom-construction';
import type {
	Media,
	MediaSet,
	MediaType
} from '@rt_lite/common/models';

const expectedFrontPageMediaSets = frontPageMediaSets
	// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
	.map(set => ({
		...set,
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		media: set.media.map(item => ({
			...item,
			url: new URL(item.url)
		}))
	}));

// eslint-disable-next-line max-lines-per-function
describe('Rotten Tomatoes Parser', () => {
	// eslint-disable-next-line @typescript-eslint/init-declarations
	let parser: Parser;

	beforeEach(() => {
		parser = createParser();
	});

	describe('parseFrontPageListItem', () => {
		test('Full record returns Media', () => {
			const listItem = document.createElement('li');
			listItem.innerHTML = `
				<a href='/planet-of-the-apes'>
					<span class='dynamic-text-list__item-title'>Planet of the Apes</span>
					<span slot='tomatometer-value'>100</span>
				</a>
			`.trim();

			const expectedOutput: Media<MediaType.MOVIE> = {
				title: 'Planet of the Apes',
				score: 100,
				url: new URL('/planet-of-the-apes', RT_URL)
			};

			const actualOutput = parser.parseFrontPageListItem(listItem);

			expect(actualOutput).toStrictEqual(expectedOutput);
		});

		test('Record without title returns null', () => {
			const listItem = document.createElement('li');
			listItem.innerHTML = `
				<a href='/planet-of-the-apes'>
					<span slot='tomatometer-value'>100</span>
				</a>
			`.trim();

			expect(parser.parseFrontPageListItem(listItem)).toBeNull();
		});

		test('Record without anchor returns null', () => {
			const listItem = document.createElement('li');
			listItem.innerHTML = `
				<span class='dynamic-text-list__item-title'>Planet of the Apes</span>
				<span slot='tomatometer-value'>100</span>
			`.trim();

			expect(parser.parseFrontPageListItem(listItem)).toBeNull();
		});

		test('Record without url returns null', () => {
			const listItem = document.createElement('li');
			listItem.innerHTML = `
				<a>
					<span class='dynamic-text-list__item-title'>Planet of the Apes</span>
					<span slot='tomatometer-value'>100</span>
				</a>
			`.trim();

			expect(parser.parseFrontPageListItem(listItem)).toBeNull();
		});

		test('Record without score returns Media without score', () => {
			const listItem = document.createElement('li');
			listItem.innerHTML = `
				<a href='/planet-of-the-apes'>
					<span class='dynamic-text-list__item-title'>Planet of the Apes</span>
				</a>
			`.trim();

			const expectedOutput: Media<MediaType.MOVIE> = {
				title: 'Planet of the Apes',
				url: new URL('/planet-of-the-apes', RT_URL)
			};

			const actualOutput = parser.parseFrontPageListItem(listItem);

			expect(actualOutput).toStrictEqual(expectedOutput);
		});

		test('Record with invalid score returns Media without score', () => {
			const listItem = document.createElement('li');
			listItem.innerHTML = `
				<a href='/planet-of-the-apes'>
					<span class='dynamic-text-list__item-title'>Planet of the Apes</span>
					<span slot='tomatometer-value'>asdf</span>
				</a>
			`.trim();

			const expectedOutput: Media<MediaType.MOVIE> = {
				title: 'Planet of the Apes',
				url: new URL('/planet-of-the-apes', RT_URL)
			};

			const actualOutput = parser.parseFrontPageListItem(listItem);

			expect(actualOutput).toStrictEqual(expectedOutput);
		});
	});

	// eslint-disable-next-line max-lines-per-function
	describe('parseFrontPageList', () => {
		// eslint-disable-next-line max-lines-per-function
		test('Complete record returns complete MediaSet', () => {
			const list = document.createElement('ul');
			list.setAttribute('slot', 'list-items');
			list.innerHTML = `
				<h2 slot='header'>Planet of the Apes Collection</h2>
				<li>
					<a href='/planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Planet of the Apes</span>
						<span slot='tomatometer-value'>86</span>
					</a>
				</li>
				<li>
					<a href='/beneath-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Beneath the Planet of the Apes</span>
						<span slot='tomatometer-value'>39</span>
					</a>
				</li>
				<li>
					<a href='/escape-from-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Escape from the Planet of the Apes</span>
						<span slot='tomatometer-value'>77</span>
					</a>
				</li>
				<li>
					<a href='/conquest-of-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Conquest of the Planet of the Apes</span>
						<span slot='tomatometer-value'>50</span>
					</a>
				</li>[
      {
        "title": "Popular Streaming Movies",
        "movies": [
          {
            "title": "The Tomorrow War",
            "url": "https://www.rottentomatoes.com/m/the_tomorrow_war",
            "score": 54
          },
          {
            "title": "Fear Street Part One: 1994",
            "url": "https://www.rottentomatoes.com/m/fear_street_part_one_1994",
            "score": 80
          },
          {
            "title": "No Sudden Move",
            "url": "https://www.rottentomatoes.com/m/no_sudden_move",
            "score": 90
          },
          {
            "title": "Luca",
            "url": "https://www.rottentomatoes.com/m/luca_2021",
            "score": 90
          },
          {
            "title": "In the Heights",
            "url": "https://www.rottentomatoes.com/m/in_the_heights_2021",
            "score": 95
          },
          {
            "title": "The Boss Baby: Family Business",
            "url": "https://www.rottentomatoes.com/m/the_boss_baby_family_business",
            "score": 47
          },
          {
            "title": "Till Death",
            "url": "https://www.rottentomatoes.com/m/till_death_2021",
            "score": 92
          },
          {
            "title": "Cruella",
            "url": "https://www.rottentomatoes.com/m/cruella",
            "score": 74
          },
          {
            "title": "The Ice Road",
            "url": "https://www.rottentomatoes.com/m/the_ice_road",
            "score": 41
          },
          {
            "title": "Werewolves Within",
            "url": "https://www.rottentomatoes.com/m/werewolves_within",
            "score": 86
          }
        ]
      },
      {
        "title": "Most Popular TV on RT ",
        "movies": [
          {
            "title": "Loki: S01",
            "url": "https://www.rottentomatoes.com/tv/loki/s01",
            "score": 92
          },
          {
            "title": "Sex/Life: S01",
            "url": "https://www.rottentomatoes.com/tv/sex_life/s01",
            "score": 29
          },
          {
            "title": "Katla: S01",
            "url": "https://www.rottentomatoes.com/tv/katla/s01",
            "score": 100
          },
          {
            "title": "Blindspotting: S01",
            "url": "https://www.rottentomatoes.com/tv/blindspotting/s01",
            "score": 100
          },
          {
            "title": "Sweet Tooth: S01",
            "url": "https://www.rottentomatoes.com/tv/sweet_tooth/s01",
            "score": 98
          },
          {
            "title": "Black Summer: S02",
            "url": "https://www.rottentomatoes.com/tv/black_summer/s02",
            "score": 100
          },
          {
            "title": "Kevin Can F... Himself: S01",
            "url": "https://www.rottentomatoes.com/tv/kevin_can_f_k_himself/s01",
            "score": 83
          },
          {
            "title": "Manifest: S03",
            "url": "https://www.rottentomatoes.com/tv/manifest/s03"
          },
          {
            "title": "Young Royals: S01",
            "url": "https://www.rottentomatoes.com/tv/young_royals/s01"
          },
          {
            "title": "Sophie: A Murder in West Cork: S01",
            "url": "https://www.rottentomatoes.com/tv/sophie_a_murder_in_west_cork/s01",
            "score": 86
          }
        ]
      },
      {
        "title": "New TV This Week",
        "movies": [
          {
            "title": "Monsters at Work: S01",
            "url": "https://www.rottentomatoes.com/tv/monsters_at_work/s01",
            "score": 75
          },
          {
            "title": "Gossip Girl: S01",
            "url": "https://www.rottentomatoes.com/tv/gossip_girl_2021/s01",
            "score": 34
          },
          {
            "title": "The Beast Must Die: S01",
            "url": "https://www.rottentomatoes.com/tv/the_beast_must_die/s01",
            "score": 100
          },
          {
            "title": "Resident Evil: Infinite Darkness: S01",
            "url": "https://www.rottentomatoes.com/tv/resident_evil_infinite_darkness/s01",
            "score": 53
          },
          {
            "title": "Shark Beach with Chris Hemsworth",
            "url": "https://www.rottentomatoes.com/m/shark_beach_with_chris_hemsworth"
          },
          {
            "title": "grown-ish: S04",
            "url": "https://www.rottentomatoes.com/tv/grown_ish/s04"
          },
          {
            "title": "I Think You Should Leave With Tim Robinson: S02",
            "url": "https://www.rottentomatoes.com/tv/i_think_you_should_leave_with_tim_robinson/s02",
            "score": 100
          },
          {
            "title": "Atypical: S04",
            "url": "https://www.rottentomatoes.com/tv/atypical/s04"
          },
          {
            "title": "Virgin River: S03",
            "url": "https://www.rottentomatoes.com/tv/virgin_river/s03"
          },
          {
            "title": "Leverage: Redemption",
            "url": "https://www.rottentomatoes.com/tv/leverage_redemption",
            "score": 88
          }
        ]
      }
    ]
				<li>
					<a href='/battle-for-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Battle for the Planet of the Apes</span>
						<span slot='tomatometer-value'>36</span>
					</a>
				</li>
			`.trim();

			const expectedOutput: MediaSet<MediaType.MOVIE> = {
				title: 'Planet of the Apes Collection',
				media: [
					{
						title: 'Planet of the Apes',
						url: new URL('planet-of-the-apes', RT_URL),
						score: 86
					},
					{
						title: 'Beneath the Planet of the Apes',
						url: new URL('beneath-the-planet-of-the-apes', RT_URL),
						score: 39
					},
					{
						title: 'Escape from the Planet of the Apes',
						url: new URL('escape-from-the-planet-of-the-apes', RT_URL),
						score: 77
					},
					{
						title: 'Conquest of the Planet of the Apes',
						url: new URL('conquest-of-the-planet-of-the-apes', RT_URL),
						score: 50
					},
					{
						title: 'Battle for the Planet of the Apes',
						url: new URL('battle-for-the-planet-of-the-apes', RT_URL),
						score: 36
					}
				]
			};

			const actualOutput = parser.parseFrontPageList(list);

			expect(actualOutput).toStrictEqual(expectedOutput);
		});

		test('Record without heading returns null', () => {
			const list = document.createElement('ul');
			list.setAttribute('slot', 'list-items');
			list.innerHTML = `
				<li>
					<a href='/planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Planet of the Apes</span>
						<span slot='tomatometer-value'>86</span>
					</a>
				</li>
				<li>
					<a href='/beneath-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Beneath the Planet of the Apes</span>
						<span slot='tomatometer-value'>39</span>
					</a>
				</li>
				<li>
					<a href='/escape-from-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Escape from the Planet of the Apes</span>
						<span slot='tomatometer-value'>77</span>
					</a>
				</li>
				<li>
					<a href='/conquest-of-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Conquest of the Planet of the Apes</span>
						<span slot='tomatometer-value'>50</span>
					</a>
				</li>
				<li>
					<a href='/battle-for-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Battle for the Planet of the Apes</span>
						<span slot='tomatometer-value'>36</span>
					</a>
				</li>
			`.trim();

			const actualOutput = parser.parseFrontPageList(list);

			expect(actualOutput).toBeNull();
		});

		test('Record without movies returns null', () => {
			const list = document.createElement('ul');
			list.setAttribute('slot', 'list-items');
			list.innerHTML = `
				<h2 slot='header'>Planet of the Apes Collection</h2>
			`.trim();

			const actualOutput = parser.parseFrontPageList(list);

			expect(actualOutput).toBeNull();
		});

		test('Record without title returns null', () => {
			const list = document.createElement('ul');
			list.setAttribute('slot', 'list-items');
			list.innerHTML = `
				<h2 slot='header'></h2>
				<li>
					<a href='/planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Planet of the Apes</span>
						<span slot='tomatometer-value'>86</span>
					</a>
				</li>
				<li>
					<a href='/beneath-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Beneath the Planet of the Apes</span>
						<span slot='tomatometer-value'>39</span>
					</a>
				</li>
				<li>
					<a href='/escape-from-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Escape from the Planet of the Apes</span>
						<span slot='tomatometer-value'>77</span>
					</a>
				</li>
				<li>
					<a href='/conquest-of-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Conquest of the Planet of the Apes</span>
						<span slot='tomatometer-value'>50</span>
					</a>
				</li>
				<li>
					<a href='/battle-for-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Battle for the Planet of the Apes</span>
						<span slot='tomatometer-value'>36</span>
					</a>
				</li>
			`.trim();

			const actualOutput = parser.parseFrontPageList(list);

			expect(actualOutput).toBeNull();
		});

		test('Record with invalid movies returns MovieSet with only valid movies', () => {
			const list = document.createElement('ul');
			list.setAttribute('slot', 'list-items');
			list.innerHTML = `
				<h2 slot='header'>Planet of the Apes Collection</h2>
				<li>
					<a href='/planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Planet of the Apes</span>
						<span slot='tomatometer-value'>86</span>
					</a>
				</li>
				<li>
					<a>
						<span class='dynamic-text-list__item-title'>Beneath the Planet of the Apes</span>
						<span slot='tomatometer-value'>39</span>
					</a>
				</li>
				<li>
					<a href='/escape-from-the-planet-of-the-apes'>
						<span class='dynamic-text-list__item-title'>Escape from the Planet of the Apes</span>
						<span slot='tomatometer-value'>77</span>
					</a>
				</li>
				<li>
					<a href='/conquest-of-the-planet-of-the-apes'>
						<span slot='tomatometer-value'>50</span>
					</a>
				</li>
				<li>
					<a>
						<span class='dynamic-text-list__item-title'>Battle for the Planet of the Apes</span>
						<span slot='tomatometer-value'>36</span>
					</a>
				</li>
			`.trim();

			const expectedOutput: MediaSet<MediaType.MOVIE> = {
				title: 'Planet of the Apes Collection',
				media: [
					{
						title: 'Planet of the Apes',
						url: new URL('planet-of-the-apes', RT_URL),
						score: 86
					},
					{
						title: 'Escape from the Planet of the Apes',
						url: new URL('escape-from-the-planet-of-the-apes', RT_URL),
						score: 77
					}
				]
			};

			const actualOutput = parser.parseFrontPageList(list);

			expect(actualOutput).toStrictEqual(expectedOutput);
		});
	});

	describe('parseDynamicListItem', () => {
		test('Returns null when no title present', () => {
			const listItem = generateDynamicListItem({ title: false });

			const media = parser.parseDynamicListItem(listItem);

			expect(media).toBeNull();
		});

		test('Returns null if no link to the item is present', () => {
			const listItem = generateDynamicListItem({ link: false });

			const media = parser.parseDynamicListItem(listItem);

			expect(media).toBeNull();
		});

		test('Returns null when title element has no textContent', () => {
			const listItem = generateDynamicListItem({ titleTextContent: false });

			const media = parser.parseDynamicListItem(listItem);

			expect(media).toBeNull();
		});

		test('Returns null if link has null href', () => {
			const listItem = generateDynamicListItem({ linkHref: false });

			const media = parser.parseDynamicListItem(listItem);

			expect(media).toBeNull();
		});

		test('Returns Media without score if no score present', () => {
			const listItem = generateDynamicListItem({ score: false });

			const media = parser.parseDynamicListItem(listItem);

			expect(media).not.toBeNull();
			expect(media?.score).toBeUndefined();
		});

		test('Returns item without score if score element missing percentage attribute', () => {
			const listItem = generateDynamicListItem({ scorePercentage: false });

			const media = parser.parseDynamicListItem(listItem);

			expect(media).not.toBeNull();
			expect(media?.score).toBeUndefined();
		});

		test('Returns Media without score if score is not a number', () => {
			const listItem = generateDynamicListItem();
			const scoreEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE)!;

			expect(scoreEl).not.toBeNull();

			scoreEl.setAttribute('criticsscore', 'asdf');

			const media = parser.parseDynamicListItem(listItem);

			expect(media).not.toBeNull();
			expect(media?.score).toBeUndefined();
		});

		test('Returns Media with score if list item contains numeric score', () => {
			const listItem = generateDynamicListItem();

			const media = parser.parseDynamicListItem(listItem);

			expect(media).not.toBeNull();
			expect(typeof media?.title).toStrictEqual('string');
			expect(media?.url).toBeInstanceOf(URL);
			expect(typeof media?.score).toStrictEqual('number');
		});
	});

	describe('parseDynamicList', () => {
		test('Returns null if no heading present', () => {
			const list = generateDynamicList({
				heading: false
			});

			const media = parser.parseDynamicList(list);

			expect(media).toBeNull();
		});

		test('Returns null if no movie list items present', () => {
			const list = generateDynamicList({
				emptyList: true
			});

			const media = parser.parseDynamicList(list);

			expect(media).toBeNull();
		});

		test('Returns null if heading contains no title', () => {
			const list = generateDynamicList({ titleTextContent: false });

			const media = parser.parseDynamicList(list);

			expect(media).toBeNull();
		});

		test('Returns MediaSet with movies containing only valid movies', () => {
			const invalidCount = 3;
			const expectedCount = DEFAULT_LIST_LENGTH - invalidCount;
			const list = generateDynamicList({ invalidCount });
			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
			const mediaSet = parser.parseDynamicList(list)!;

			expect(mediaSet).not.toBeNull();
			expect(mediaSet.media).toHaveLength(expectedCount);
		});
	});

	describe('parseFrontPage', () => {
		test('Returns empty array if no valid media sets on page', () => {
			const body = document.createElement('body');

			const mediaSets = parser.parseFrontPage(body);

			expect(Array.isArray(mediaSets)).toStrictEqual(true);
			expect(mediaSets).toHaveLength(0);
		});

		test('Returns array containing all valid media sets on page', async () => {
			expect.assertions(1);

			const frontPagePath = path.resolve(__dirname, '../testing/front-page.html');
			const frontPage = await readFile(frontPagePath, 'utf-8');
			const bodyHtml = trimNonBodyHtml(frontPage);
			const body = document.createElement('body');
			body.innerHTML = bodyHtml;

			const mediaSets = parser.parseFrontPage(body);

			expect(mediaSets).toStrictEqual(expectedFrontPageMediaSets);
		});
	});
});
