import { createTVSeriesFromRTTVSeries } from '~/factories/tv-series-from-rt-tv-series';
import { RT_URL } from '~/core/rt-url';
import type { RTTVSeries } from '~/models/rt-tv-series';

describe('createTVSeriesFromRTTVSeries', () => {
	const rtTvSeriesTemplate = {
		meterScore: 55,
		startYear: 1974,
		title: 'Planet of the Apes',
		url: '/tv/planet-of-the-apes'
	};

	test('Converts full rtTvSeries to full tvSeries', () => {
		const rtTvSeries = { ...rtTvSeriesTemplate };

		const expected = {
			score: rtTvSeries.meterScore,
			title: rtTvSeries.title,
			url: new URL(rtTvSeries.url, RT_URL),
			year: rtTvSeries.startYear
		};

		const converted = createTVSeriesFromRTTVSeries(rtTvSeries);

		expect(converted).toStrictEqual(expected);
	});

	test('rtTvSeries with invalid startYear converts to tvSeries without year', () => {
		const rtTvSeries = { ...rtTvSeriesTemplate, startYear: 'asdf' };

		const expected = {
			title: rtTvSeries.title,
			url: new URL(rtTvSeries.url, RT_URL),
			score: rtTvSeries.meterScore
		};

		const converted = createTVSeriesFromRTTVSeries(rtTvSeries as unknown as RTTVSeries);

		expect(converted).toStrictEqual(expected);
	});

	test('rtTvSeries with null startYear converts to tvSeries without year', () => {
		const rtTvSeries = { ...rtTvSeriesTemplate, startYear: null };

		const expected = {
			title: rtTvSeries.title,
			url: new URL(rtTvSeries.url, RT_URL),
			score: rtTvSeries.meterScore
		};

		const converted = createTVSeriesFromRTTVSeries(rtTvSeries);

		expect(converted).toStrictEqual(expected);
	});

	test('rtTvSeries with invalid score converts to tvSeries without score', () => {
		const rtTvSeries = { ...rtTvSeriesTemplate, meterScore: 'asdf' };

		const expected = {
			title: rtTvSeries.title,
			url: new URL(rtTvSeries.url, RT_URL),
			year: rtTvSeries.startYear
		};

		const converted = createTVSeriesFromRTTVSeries(rtTvSeries as unknown as RTTVSeries);

		expect(converted).toStrictEqual(expected);
	});

	test('rtTvSeries with null score converts to tvSeries without score', () => {
		const rtTvSeries = { ...rtTvSeriesTemplate, meterScore: null };

		const expected = {
			title: rtTvSeries.title,
			url: new URL(rtTvSeries.url, RT_URL),
			year: rtTvSeries.startYear
		};

		const converted = createTVSeriesFromRTTVSeries(rtTvSeries);

		expect(converted).toStrictEqual(expected);
	});
});
