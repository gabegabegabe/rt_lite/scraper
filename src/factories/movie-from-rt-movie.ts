import type { Movie } from '@rt_lite/common/models';
import { RT_URL } from '~/core/rt-url';
import type { RTMovie } from '~/models/rt-movie';

export const createMovieFromRTMovie = ({
	meterScore,
	name,
	url,
	year
}: Readonly<RTMovie>): Movie => {
	const movie: Movie = {
		title: name,
		url: new URL(url, RT_URL)
	};

	if (meterScore !== null && !isNaN(meterScore)) movie.score = meterScore;
	if (year !== null && !isNaN(year)) movie.year = year;

	return movie;
};
