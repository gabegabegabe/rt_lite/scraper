import { createMovieFromRTMovie } from '~/factories/movie-from-rt-movie';
import { RT_URL } from '~/core/rt-url';
import type { RTMovie } from '~/models/rt-movie';

describe('createMovieFromRTMovie', () => {
	const rtMovieTemplate = {
		meterScore: 90,
		name: 'Planet of the Apes',
		url: '/planet-of-the-apes',
		year: 1968
	};

	test('Converts full rtMovie to full movie', () => {
		const rtMovie = { ...rtMovieTemplate };

		const expected = {
			score: rtMovie.meterScore,
			title: rtMovie.name,
			url: new URL(rtMovie.url, RT_URL),
			year: rtMovie.year
		};

		const converted = createMovieFromRTMovie(rtMovie);

		expect(converted).toStrictEqual(expected);
	});

	test('rtMovie with invalid year converts to movie without year', () => {
		const rtMovie = { ...rtMovieTemplate, year: 'asdf' };

		const expected = {
			title: rtMovie.name,
			url: new URL(rtMovie.url, RT_URL),
			score: rtMovie.meterScore
		};

		const converted = createMovieFromRTMovie(rtMovie as unknown as RTMovie);

		expect(converted).toStrictEqual(expected);
	});

	test('rtMovie with null year converts to movie without year', () => {
		const rtMovie = { ...rtMovieTemplate, year: null };

		const expected = {
			title: rtMovie.name,
			url: new URL(rtMovie.url, RT_URL),
			score: rtMovie.meterScore
		};

		const converted = createMovieFromRTMovie(rtMovie);

		expect(converted).toStrictEqual(expected);
	});

	test('rtMovie with invalid score converts to movie without score', () => {
		const rtMovie = { ...rtMovieTemplate, meterScore: 'asdf' };

		const expected = {
			title: rtMovie.name,
			url: new URL(rtMovie.url, RT_URL),
			year: rtMovie.year
		};

		const converted = createMovieFromRTMovie(rtMovie as unknown as RTMovie);

		expect(converted).toStrictEqual(expected);
	});

	test('rtMovie with null score converts to movie without score', () => {
		const rtMovie = { ...rtMovieTemplate, meterScore: null };

		const expected = {
			title: rtMovie.name,
			url: new URL(rtMovie.url, RT_URL),
			year: rtMovie.year
		};

		const converted = createMovieFromRTMovie(rtMovie);

		expect(converted).toStrictEqual(expected);
	});
});
