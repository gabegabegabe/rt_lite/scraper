import { createLogger } from '~/factories/logger';
import { getRandomArgs } from '~/testing/random-args';

// eslint-disable-next-line @typescript-eslint/no-empty-function
const doNothing = (): void => {};

// eslint-disable-next-line max-lines-per-function
describe('Logger class', () => {
	// eslint-disable-next-line @typescript-eslint/init-declarations
	let args: (boolean | number | string)[];

	beforeEach(() => {
		args = getRandomArgs();
	});

	describe('General with default logging service', () => {
		const logger = createLogger();

		afterEach(() => {
			args = getRandomArgs();
		});

		test('Log calls log', () => {
			const logSpy = jest
				.spyOn(console, 'log')
				.mockImplementation(doNothing);

			logger.log(...args);

			expect(logSpy).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});

		test('Error calls error', () => {
			const errorSpy = jest
				.spyOn(console, 'error')
				.mockImplementation(doNothing);

			logger.error(...args);

			expect(errorSpy).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});

		test('Info calls info', () => {
			const infoSpy = jest
				.spyOn(console, 'info')
				.mockImplementation(doNothing);

			logger.info(...args);

			expect(infoSpy).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});

		test('Warn calls warn', () => {
			const warnSpy = jest
				.spyOn(console, 'warn')
				.mockImplementation(doNothing);

			logger.warn(...args);

			expect(warnSpy).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});
	});

	describe('General with specified logging service', () => {
		const mockLoggingService = {
			error: jest.fn(),
			info: jest.fn(),
			log: jest.fn(),
			warn: jest.fn()
		};

		const logger = createLogger(mockLoggingService);

		afterEach(() => {
			// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
			Object.values(mockLoggingService).forEach(mockFn => mockFn.mockClear());
			args = getRandomArgs();
		});

		test('Log calls log', () => {
			logger.log(...args);

			expect(mockLoggingService.log).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});

		test('Error calls error', () => {
			logger.error(...args);

			expect(mockLoggingService.error).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});

		test('Info calls info', () => {
			logger.info(...args);

			expect(mockLoggingService.info).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});

		test('Warn calls warn', () => {
			logger.warn(...args);

			expect(mockLoggingService.warn).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});
	});

	describe('Without success function in logging service', () => {
		const mockLoggingService = {
			error: jest.fn(),
			info: jest.fn(),
			log: jest.fn(),
			warn: jest.fn()
		};

		const logger = createLogger(mockLoggingService);

		afterEach(() => {
			// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
			Object.values(mockLoggingService).forEach(mockFn => mockFn.mockClear());
			args = getRandomArgs();
		});

		test('Success calls log', () => {
			logger.success(...args);

			expect(mockLoggingService.log).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});
	});

	describe('With success function in logging service', () => {
		const mockLoggingService = {
			error: jest.fn(),
			info: jest.fn(),
			log: jest.fn(),
			success: jest.fn(),
			warn: jest.fn()
		};

		const logger = createLogger(mockLoggingService);

		afterEach(() => {
			// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
			Object.values(mockLoggingService).forEach(mockFn => mockFn.mockClear());
			args = getRandomArgs();
		});

		test('Success calls success', () => {
			logger.success(...args);

			expect(mockLoggingService.log).not.toHaveBeenCalled();
			expect(mockLoggingService.success).toHaveBeenCalledWith(args[0], ...args.slice(1));
		});
	});
});
