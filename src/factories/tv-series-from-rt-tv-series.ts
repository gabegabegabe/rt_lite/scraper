import { RT_URL } from '~/core/rt-url';
import type { RTTVSeries } from '~/models/rt-tv-series';
import type { TVSeries } from '@rt_lite/common/models';

export const createTVSeriesFromRTTVSeries = ({
	meterScore,
	startYear,
	title,
	url
}: Readonly<RTTVSeries>): TVSeries => {
	const series: TVSeries = {
		title,
		url: new URL(url, RT_URL)
	};

	if (meterScore !== null && !isNaN(meterScore)) series.score = meterScore;
	if (startYear !== null && !isNaN(startYear)) series.year = startYear;

	return series;
};
