export const DEFAULT_LIST_LENGTH = 10;

export type DynamicListOptions = {
	heading?: boolean;
	emptyList?: boolean;
	titleTextContent?: boolean;
	invalidCount?: number;
};

export type DynamicListItemOptions = {
	title?: boolean;
	titleTextContent?: boolean;
	link?: boolean;
	linkHref?: boolean;
	score?: boolean;
	scorePercentage?: boolean;
};

const DEFAULT_DYNAMIC_LIST_OPTIONS: DynamicListOptions = {
	emptyList: false,
	heading: true,
	invalidCount: 0,
	titleTextContent: true
};

const DEFAULT_DYNAMIC_LIST_ITEM_OPTIONS: DynamicListItemOptions = {
	link: true,
	linkHref: true,
	score: true,
	scorePercentage: true,
	title: true,
	titleTextContent: true
};

export const generateDynamicListItem = ({
	title = DEFAULT_DYNAMIC_LIST_ITEM_OPTIONS.title,
	titleTextContent = DEFAULT_DYNAMIC_LIST_ITEM_OPTIONS.titleTextContent,
	link = DEFAULT_DYNAMIC_LIST_ITEM_OPTIONS.link,
	linkHref = DEFAULT_DYNAMIC_LIST_ITEM_OPTIONS.linkHref,
	score = DEFAULT_DYNAMIC_LIST_ITEM_OPTIONS.score,
	scorePercentage = DEFAULT_DYNAMIC_LIST_ITEM_OPTIONS.scorePercentage
}: Readonly<DynamicListItemOptions> = DEFAULT_DYNAMIC_LIST_ITEM_OPTIONS): HTMLDivElement => {
	const listItem = document.createElement('tile-dynamic') as HTMLDivElement;

	if (title === true) {
		const titleWrapper = document.createElement('button');
		titleWrapper.classList.add('js-show-modal-trailer');

		const titleEl = document.createElement('span');
		titleEl.classList.add('sr-only');

		titleWrapper.append(titleEl);

		if (titleTextContent === true) titleEl.textContent = 'Planet of the Apes';

		listItem.append(titleWrapper);
	}

	if (link === true) {
		const linkEl = document.createElement('a');
		linkEl.setAttribute('slot', 'caption');

		if (linkHref === true) linkEl.setAttribute('href', '/planet-of-the-apes');

		listItem.append(linkEl);
	}

	if (score === true) {
		const scoreEl = document.createElement('score-pairs') as HTMLDivElement;

		if (scorePercentage === true) scoreEl.setAttribute('criticsscore', '90');

		listItem.append(scoreEl);
	}

	return listItem;
};

export const generateDynamicList = ({
	heading = DEFAULT_DYNAMIC_LIST_OPTIONS.heading,
	emptyList = DEFAULT_DYNAMIC_LIST_OPTIONS.emptyList,
	titleTextContent = DEFAULT_DYNAMIC_LIST_OPTIONS.titleTextContent,
	invalidCount = DEFAULT_DYNAMIC_LIST_OPTIONS.invalidCount
}: Readonly<DynamicListOptions> = DEFAULT_DYNAMIC_LIST_OPTIONS): HTMLDivElement => {
	const list = document.createElement('section') as HTMLDivElement;
	list.classList.add('dynamic-poster-list');

	if (heading === true) {
		const headingEl = document.createElement('h2');

		if (titleTextContent === true) headingEl.textContent = 'New Releases';

		list.append(headingEl);
	}

	if (emptyList === true) return list;

	const listItems: HTMLDivElement[] = new Array(DEFAULT_LIST_LENGTH)
		.fill(null)
		.map(() => generateDynamicListItem());

	if (typeof invalidCount === 'number') for (let index = 0; index < invalidCount; ++index) listItems[index].innerHTML = '';

	// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
	listItems.forEach(listItem => { list.append(listItem); });

	return list;
};

