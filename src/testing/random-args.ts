import { faker } from '@faker-js/faker';
import { getRandomInt } from '~/utilities/random';

const MAX_NUM_ARGS = 10;

export const getRandomArgs = (max = MAX_NUM_ARGS): (boolean | number | string)[] => {
	const numArgs = getRandomInt(1, max);

	return new Array(numArgs)
		.fill(null)
		.map((): boolean | number | string => {
			const numOptions = 2;
			const num = getRandomInt(0, numOptions);

			switch (num) {
				case 1:
					return faker.person.firstName();
				case numOptions:
					return getRandomInt();
				default:
					// eslint-disable-next-line @typescript-eslint/no-magic-numbers
					return Math.random() > 0.5;
			}
		});
};
