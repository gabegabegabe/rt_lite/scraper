import { RtElTarget } from '~/models/rt-el-target';
import {
	DEFAULT_LIST_LENGTH,
	generateDynamicList,
	generateDynamicListItem
} from '~/testing/rt-dom-construction';
import {
	describe,
	expect,
	test
} from '@jest/globals';

// eslint-disable-next-line max-lines-per-function
describe('RT Dom Construction', () => {
	describe('Dynamic list generator', () => {
		test('Generates fully valid list by default', () => {
			const list = generateDynamicList();

			expect(list).not.toBeNull();
			expect(list.matches(RtElTarget.FRONT_PAGE_DYNAMIC_LIST)).toStrictEqual(true);

			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
			const headingEl = list.querySelector<HTMLHeadingElement>(RtElTarget.DYNAMIC_LIST_TITLE)!;

			expect(headingEl).not.toBeNull();
			expect(headingEl.textContent).not.toBeNull();
			expect(headingEl.textContent?.length).toBeGreaterThan(0);

			const listItemEls = list.querySelectorAll<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM);

			expect(listItemEls).toHaveLength(DEFAULT_LIST_LENGTH);
		});

		test('Generates without heading if specified', () => {
			const list = generateDynamicList({ heading: false });

			expect(list).not.toBeNull();
			expect(list.matches(RtElTarget.FRONT_PAGE_DYNAMIC_LIST)).toStrictEqual(true);

			const headingEl = list.querySelector<HTMLHeadingElement>(RtElTarget.DYNAMIC_LIST_TITLE);

			expect(headingEl).toBeNull();

			const listItemEls = list.querySelectorAll<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM);

			expect(listItemEls).toHaveLength(DEFAULT_LIST_LENGTH);
		});

		test('Generates heading without text if specified', () => {
			const list = generateDynamicList({ titleTextContent: false });

			expect(list).not.toBeNull();
			expect(list.matches(RtElTarget.FRONT_PAGE_DYNAMIC_LIST)).toStrictEqual(true);

			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
			const headingEl = list.querySelector<HTMLHeadingElement>(RtElTarget.DYNAMIC_LIST_TITLE)!;

			expect(headingEl).not.toBeNull();
			expect(headingEl.textContent).not.toBeNull();
			expect(headingEl.textContent).toHaveLength(0);

			const listItemEls = list.querySelectorAll<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM);

			expect(listItemEls).toHaveLength(DEFAULT_LIST_LENGTH);
		});

		test('Generates empty list if specified', () => {
			const list = generateDynamicList({ emptyList: true });

			expect(list).not.toBeNull();
			expect(list.matches(RtElTarget.FRONT_PAGE_DYNAMIC_LIST)).toStrictEqual(true);

			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
			const headingEl = list.querySelector<HTMLHeadingElement>(RtElTarget.DYNAMIC_LIST_TITLE)!;

			expect(headingEl).not.toBeNull();
			expect(headingEl.textContent).not.toBeNull();
			expect(headingEl.textContent?.length).toBeGreaterThan(0);

			const listItemEls = list.querySelectorAll<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM);

			expect(listItemEls).toHaveLength(0);
		});

		test('Generates list with n invalid entries if specified', () => {
			const invalidCount = 3;
			const list = generateDynamicList({ invalidCount });

			expect(list).not.toBeNull();
			expect(list.matches(RtElTarget.FRONT_PAGE_DYNAMIC_LIST)).toStrictEqual(true);

			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
			const headingEl = list.querySelector<HTMLHeadingElement>(RtElTarget.DYNAMIC_LIST_TITLE)!;

			expect(headingEl).not.toBeNull();
			expect(headingEl.textContent).not.toBeNull();
			expect(headingEl.textContent?.length).toBeGreaterThan(0);

			const listItemEls = [...list.querySelectorAll<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM)]
				// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
				.filter(({ innerHTML }) => innerHTML.length > 0);

			expect(listItemEls).toHaveLength(DEFAULT_LIST_LENGTH - invalidCount);
		});
	});

	// eslint-disable-next-line max-lines-per-function
	describe('Dynamic list item generator', () => {
		test('Generates complete list item by default', () => {
			const listItem = generateDynamicListItem();

			expect(listItem).not.toBeNull();
			expect(listItem.matches(RtElTarget.DYNAMIC_LIST_ITEM)).toStrictEqual(true);

			const titleEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_TITLE);

			expect(titleEl).not.toBeNull();
			expect(typeof titleEl?.textContent).toStrictEqual('string');
			expect(titleEl?.textContent?.length).toBeGreaterThan(0);

			const linkEl = listItem.querySelector<HTMLAnchorElement>(RtElTarget.DYNAMIC_LIST_ITEM_LINK);

			expect(linkEl).not.toBeNull();
			expect(linkEl?.tagName).toStrictEqual('A');
			expect(linkEl?.getAttribute('href')).not.toBeNull();
			expect(linkEl?.href.length).toBeGreaterThan(0);

			const scoreEl = listItem.querySelector<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE);

			expect(scoreEl).not.toBeNull();
			expect(typeof scoreEl?.getAttribute('criticsscore')).toStrictEqual('string');
			expect(isNaN((scoreEl?.getAttribute('criticsscore') as unknown) as number)).toStrictEqual(false);
		});

		test('Generates without title if specified', () => {
			const listItem = generateDynamicListItem({ title: false });

			expect(listItem).not.toBeNull();
			expect(listItem.matches(RtElTarget.DYNAMIC_LIST_ITEM)).toStrictEqual(true);

			const titleEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_TITLE);

			expect(titleEl).toBeNull();

			const linkEl = listItem.querySelector<HTMLAnchorElement>(RtElTarget.DYNAMIC_LIST_ITEM_LINK);

			expect(linkEl).not.toBeNull();
			expect(linkEl?.tagName).toStrictEqual('A');
			expect(linkEl?.getAttribute('href')).not.toBeNull();
			expect(linkEl?.href.length).toBeGreaterThan(0);

			const scoreEl = listItem.querySelector<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE);

			expect(scoreEl).not.toBeNull();
			expect(typeof scoreEl?.getAttribute('criticsscore')).toStrictEqual('string');
			expect(isNaN((scoreEl?.getAttribute('criticsscore') as unknown) as number)).toStrictEqual(false);
		});

		test('Generates without title text if specified', () => {
			const listItem = generateDynamicListItem({ titleTextContent: false });

			expect(listItem).not.toBeNull();
			expect(listItem.matches(RtElTarget.DYNAMIC_LIST_ITEM)).toStrictEqual(true);

			const titleEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_TITLE);

			expect(titleEl).not.toBeNull();
			expect(typeof titleEl?.textContent).toStrictEqual('string');
			expect(titleEl?.textContent?.length).toStrictEqual(0);

			const linkEl = listItem.querySelector<HTMLAnchorElement>(RtElTarget.DYNAMIC_LIST_ITEM_LINK);

			expect(linkEl).not.toBeNull();
			expect(linkEl?.tagName).toStrictEqual('A');
			expect(linkEl?.getAttribute('href')).not.toBeNull();
			expect(linkEl?.href.length).toBeGreaterThan(0);

			const scoreEl = listItem.querySelector<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE);

			expect(scoreEl).not.toBeNull();
			expect(typeof scoreEl?.getAttribute('criticsscore')).toStrictEqual('string');
			expect(isNaN((scoreEl?.getAttribute('criticsscore') as unknown) as number)).toStrictEqual(false);
		});

		test('Generates without link if specified', () => {
			const listItem = generateDynamicListItem({ link: false });

			expect(listItem).not.toBeNull();
			expect(listItem.matches(RtElTarget.DYNAMIC_LIST_ITEM)).toStrictEqual(true);

			const titleEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_TITLE);

			expect(titleEl).not.toBeNull();
			expect(typeof titleEl?.textContent).toStrictEqual('string');
			expect(titleEl?.textContent?.length).toBeGreaterThan(0);

			const linkEl = listItem.querySelector<HTMLAnchorElement>(RtElTarget.DYNAMIC_LIST_ITEM_LINK);

			expect(linkEl).toBeNull();

			const scoreEl = listItem.querySelector<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE);

			expect(scoreEl).not.toBeNull();
			expect(typeof scoreEl?.getAttribute('criticsscore')).toStrictEqual('string');
			expect(isNaN((scoreEl?.getAttribute('criticsscore') as unknown) as number)).toStrictEqual(false);
		});

		test('Generates without link href if specified', () => {
			const listItem = generateDynamicListItem({ linkHref: false });

			expect(listItem).not.toBeNull();
			expect(listItem.matches(RtElTarget.DYNAMIC_LIST_ITEM)).toStrictEqual(true);

			const titleEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_TITLE);

			expect(titleEl).not.toBeNull();
			expect(typeof titleEl?.textContent).toStrictEqual('string');
			expect(titleEl?.textContent?.length).toBeGreaterThan(0);

			const linkEl = listItem.querySelector<HTMLAnchorElement>(RtElTarget.DYNAMIC_LIST_ITEM_LINK);

			expect(linkEl).not.toBeNull();
			expect(linkEl?.tagName).toStrictEqual('A');
			expect(linkEl?.getAttribute('href')).toBeNull();

			const scoreEl = listItem.querySelector<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE);

			expect(scoreEl).not.toBeNull();
			expect(typeof scoreEl?.getAttribute('criticsscore')).toStrictEqual('string');
			expect(isNaN((scoreEl?.getAttribute('criticsscore') as unknown) as number)).toStrictEqual(false);
		});

		test('Generates without score if specified', () => {
			const listItem = generateDynamicListItem({ score: false });

			expect(listItem).not.toBeNull();
			expect(listItem.matches(RtElTarget.DYNAMIC_LIST_ITEM)).toStrictEqual(true);

			const titleEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_TITLE);

			expect(titleEl).not.toBeNull();
			expect(typeof titleEl?.textContent).toStrictEqual('string');
			expect(titleEl?.textContent?.length).toBeGreaterThan(0);

			const linkEl = listItem.querySelector<HTMLAnchorElement>(RtElTarget.DYNAMIC_LIST_ITEM_LINK);

			expect(linkEl).not.toBeNull();
			expect(linkEl?.tagName).toStrictEqual('A');
			expect(linkEl?.getAttribute('href')).not.toBeNull();
			expect(linkEl?.href.length).toBeGreaterThan(0);

			const scoreEl = listItem.querySelector<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE);

			expect(scoreEl).toBeNull();
		});

		test('Generates without score percentage if specified', () => {
			const listItem = generateDynamicListItem({ scorePercentage: false });

			expect(listItem).not.toBeNull();
			expect(listItem.matches(RtElTarget.DYNAMIC_LIST_ITEM)).toStrictEqual(true);

			const titleEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_TITLE);

			expect(titleEl).not.toBeNull();
			expect(typeof titleEl?.textContent).toStrictEqual('string');
			expect(titleEl?.textContent?.length).toBeGreaterThan(0);

			const linkEl = listItem.querySelector<HTMLAnchorElement>(RtElTarget.DYNAMIC_LIST_ITEM_LINK);

			expect(linkEl).not.toBeNull();
			expect(linkEl?.tagName).toStrictEqual('A');
			expect(linkEl?.getAttribute('href')).not.toBeNull();
			expect(linkEl?.href.length).toBeGreaterThan(0);

			const scoreEl = listItem.querySelector<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE);

			expect(scoreEl).not.toBeNull();
			expect(scoreEl?.getAttribute('criticsscore')).toBeNull();
		});

		test('Returns empty div if all items disabled', () => {
			const listItem = generateDynamicListItem({
				title: false,
				link: false,
				score: false
			});

			expect(listItem).not.toBeNull();
			expect(listItem.matches(RtElTarget.DYNAMIC_LIST_ITEM)).toStrictEqual(true);

			const titleEl = listItem.querySelector<HTMLSpanElement>(RtElTarget.DYNAMIC_LIST_ITEM_TITLE);

			expect(titleEl).toBeNull();

			const linkEl = listItem.querySelector<HTMLAnchorElement>(RtElTarget.DYNAMIC_LIST_ITEM_LINK);

			expect(linkEl).toBeNull();

			const scoreEl = listItem.querySelector<HTMLDivElement>(RtElTarget.DYNAMIC_LIST_ITEM_SCORE);

			expect(scoreEl).toBeNull();
		});
	});
});
