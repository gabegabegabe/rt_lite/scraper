export const trimNonBodyHtml = (html: string): string => {
	const start = '<body';
	const end = '</body>';

	const bodyHtml = html
		.substring(html.indexOf(start) + start.length, html.indexOf(end))
		.replace(/^[^>]*>/u, '');

	return bodyHtml;
};
