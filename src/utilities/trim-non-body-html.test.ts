import { trimNonBodyHtml } from '~/utilities/trim-non-body-html';

describe('trimNonBodyHtml', () => {
	test('Body is extracted correctly from typical page', () => {
		const expectedOutput = '<h1>Hello world!</h1>';
		const page = `
			<!doctype html>
			<html lang='en'>
				<head>
					<meta charset='utf-8' />
					<title>Dummy Page</title>
				</head>
				<body>
					${expectedOutput}
				</body>
			</html>
		`.trim()
			.replace(/\n|\t/gu, '');

		const actualOutput = trimNonBodyHtml(page);

		expect(actualOutput).toBe(expectedOutput);
	});

	test('Body is extract correctly from page with only body', () => {
		const expectedOutput = '<h1>Nobody</h1>';
		const page = `
			<body>
				${expectedOutput}
			</body>
		`.trim()
			.replace(/\n|\t/gu, '');

		const actualOutput = trimNonBodyHtml(page);

		expect(actualOutput).toBe(expectedOutput);
	});
});
