const {
	MAX_SAFE_INTEGER,
	MIN_SAFE_INTEGER
} = Number;

export const getRandomInt = (min: number = MIN_SAFE_INTEGER, max: number = MAX_SAFE_INTEGER): number => {
	const realMin = Math.ceil(min);
	const realMax = Math.floor(max);

	const rand = Math.random() * (realMax - realMin + 1);

	return Math.floor(rand + realMin);
};

export const getRandomFromArray = <T> (arr: readonly T[]): T => {
	const min = 0;
	const max = arr.length - 1;

	return arr[getRandomInt(min, max)];
};
