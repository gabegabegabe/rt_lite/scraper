export const exists = <T> (value?: T | null): value is T => {
	// eslint-disable-next-line no-undefined
	if (value === undefined) return false;
	if (value === null) return false;

	return true;
};
