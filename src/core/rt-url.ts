/**
 * The URL for Rotten Tomatoes
 *
 * @internal
 */
export const RT_URL = new URL('https://www.rottentomatoes.com');
