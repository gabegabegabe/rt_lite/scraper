import { RT_URL } from '~/core/rt-url';

/**
 * The URL for performing a search on Rotten Tomatoes
 *
 * @internal
 */
export const RT_SEARCH_URL = new URL('/napi/search', RT_URL);
