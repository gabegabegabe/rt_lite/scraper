import { createScraper as createGenericScraper } from '~/factories/scraper';
import { createLogger } from '~/factories/logger';
import type { Immutable } from '~/models/immutable';
import type { Logger } from '~/models/logger';
import type { Scraper } from '~/models/scraper';

const bodyProducer = (html: string): HTMLBodyElement => {
	const body = document.createElement('body');
	body.innerHTML = html;

	return body;
};

const fetcher = fetch.bind(window);

export const createScraper = ({
	logger = console,
	shouldLog = true
}: Immutable<{
	logger?: Partial<Logger> & Pick<Logger, 'log'>;
	shouldLog?: boolean;
}> = {
	logger: console,
	shouldLog: true
}): Scraper => createGenericScraper({
	bodyProducer,
	fetcher,
	logger: createLogger(logger),
	shouldLog
});
