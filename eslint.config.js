import base from '@gabegabegabe/eslint-config';
import browser from '@gabegabegabe/eslint-config/browser';
import jestTs from '@gabegabegabe/eslint-config/jest-typescript';
import js from '@gabegabegabe/eslint-config/javascript';
import json from '@gabegabegabe/eslint-config/json';
import node from '@gabegabegabe/eslint-config/node';
import ts from '@gabegabegabe/eslint-config/typescript';

const TEST_FILES = ['src/testing/', '**/*.test.ts', '**/*.mock.ts'];

export default [
	base,
	{
		ignores: [
			'coverage/',
			'dist/',
			'documentation/',
			'node_modules/'
		]
	},
	{
		files: ['*.js', '*.cjs', ...TEST_FILES],
		...node
	},
	{
		files: ['*.js', '*.cjs'],
		...js
	},
	{
		files: ['src/**/*'],
		...browser
	},
	{
		files: ['**/*.json'],
		...json
	},
	{
		files: ['src/**/*.js', 'src/**/*.ts'],
		...ts
	},
	{
		files: [...TEST_FILES],
		...jestTs
	}
];
